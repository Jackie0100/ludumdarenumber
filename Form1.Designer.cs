﻿namespace LDBot
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripNotify = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemNextGame = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOpenQueue = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCloseQueue = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemClearlist = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOpenGameUrl = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripNotify.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStripNotify;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "1";
            this.notifyIcon1.Visible = true;
            // 
            // contextMenuStripNotify
            // 
            this.contextMenuStripNotify.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemNextGame,
            this.toolStripMenuItemOpenGameUrl,
            this.toolStripMenuItemOpenQueue,
            this.toolStripMenuItemCloseQueue,
            this.toolStripMenuItemClearlist,
            this.toolStripMenuItemExit});
            this.contextMenuStripNotify.Name = "contextMenuStripNotify";
            this.contextMenuStripNotify.Size = new System.Drawing.Size(156, 136);
            this.contextMenuStripNotify.Text = "Next Game";
            // 
            // toolStripMenuItemNextGame
            // 
            this.toolStripMenuItemNextGame.Name = "toolStripMenuItemNextGame";
            this.toolStripMenuItemNextGame.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItemNextGame.Text = "Next Game";
            // 
            // toolStripMenuItemOpenQueue
            // 
            this.toolStripMenuItemOpenQueue.Name = "toolStripMenuItemOpenQueue";
            this.toolStripMenuItemOpenQueue.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItemOpenQueue.Text = "Open Queue";
            // 
            // toolStripMenuItemCloseQueue
            // 
            this.toolStripMenuItemCloseQueue.Name = "toolStripMenuItemCloseQueue";
            this.toolStripMenuItemCloseQueue.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItemCloseQueue.Text = "Close Queue";
            // 
            // toolStripMenuItemClearlist
            // 
            this.toolStripMenuItemClearlist.Name = "toolStripMenuItemClearlist";
            this.toolStripMenuItemClearlist.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItemClearlist.Text = "Clear List";
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItemExit.Text = "Exit";
            // 
            // toolStripMenuItemOpenGameUrl
            // 
            this.toolStripMenuItemOpenGameUrl.Name = "toolStripMenuItemOpenGameUrl";
            this.toolStripMenuItemOpenGameUrl.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItemOpenGameUrl.Text = "Open Game Url";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(120, 0);
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStripNotify.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripNotify;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemNextGame;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemClearlist;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOpenQueue;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCloseQueue;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOpenGameUrl;
    }
}

