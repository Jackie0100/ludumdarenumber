﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using TwitchLib;
using TwitchLib.Api;
using TwitchLib.Client;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;

namespace LDBot
{
    public partial class Form1 : Form
    {
        string ludumdarenumber = "46";
        string twitchchannelname = "";
        string twitchoauthtoken = "";
        string mygameUrl = "";

        TwitchClient Client { get; set; }
        List<LdGame> GameQueue { get; set; }
        LdGame CurrentGame { get; set; }
        bool IsQueueOpen { get; set; }


        public Form1()
        {
            InitializeComponent();

            toolStripMenuItemNextGame.Click += OnNextGameClicked;
            toolStripMenuItemOpenGameUrl.Click += OnOpenGameUrlClicked;
            toolStripMenuItemOpenQueue.Click += OnOpenQueueClicked;
            toolStripMenuItemCloseQueue.Click += OnCloseQueueClicked;
            toolStripMenuItemClearlist.Click += OnClearListClicked;
            toolStripMenuItemExit.Click += OnExitClicked;

            GameQueue = DeserializeObject<List<LdGame>>(Properties.Settings.Default.SaveListXml);
            if (GameQueue == null)
            {
                GameQueue = new List<LdGame>();
            }

            Client = new TwitchClient();
            Client.Initialize(new ConnectionCredentials(twitchchannelname, twitchoauthtoken), twitchchannelname);
            Client.OnChatCommandReceived += Client_OnChatCommandReceived;
            Client.Connect();

        }

        private void OnOpenGameUrlClicked(object sender, EventArgs e)
        {
            if (CurrentGame != null)
            {
                System.Diagnostics.Process.Start(CurrentGame.Url);
            }
        }

        private void OnExitClicked(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OnClearListClicked(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Delete the current queue?", "Warning!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                GameQueue.Clear();
                Properties.Settings.Default.SaveListXml = SerializeObject(GameQueue);
                Properties.Settings.Default.Save();
            }
        }

        private void OnCloseQueueClicked(object sender, EventArgs e)
        {
            IsQueueOpen = false;
            Client.SendMessage(Client.JoinedChannels[0], "The queue is now closed for submissions!");
        }

        private void OnOpenQueueClicked(object sender, EventArgs e)
        {
            IsQueueOpen = true;
            Client.SendMessage(Client.JoinedChannels[0], "The queue is now open for submissions!");
        }

        private void OnNextGameClicked(object sender, EventArgs e)
        {
            if (GameQueue.Count != 0)
            {
                CurrentGame = GameQueue[0];
                GameQueue.Remove(CurrentGame);
                AnnounceCurrentGame();
                Properties.Settings.Default.SaveListXml = SerializeObject(GameQueue);
                Properties.Settings.Default.Save();
            }
        }

        private void Client_OnChatCommandReceived(object sender, OnChatCommandReceivedArgs e)
        {
            switch (e.Command.CommandText.ToLower())
            {
                case "add":
                case "submit":
                    if (IsQueueOpen)
                    {
                        if (e.Command.ArgumentsAsList.Count != 0)
                        {
                            SubmitGame(e.Command.ArgumentsAsList[0], e.Command.ChatMessage.Username);
                            Properties.Settings.Default.SaveListXml = SerializeObject(GameQueue);
                            Properties.Settings.Default.Save();
                        }
                        else
                        {
                            Client.SendMessage(Client.JoinedChannels[0], "@" + e.Command.ChatMessage.Username + " it seems you have submitted a invalid URL, the url MUST be a Ldjam" + ludumdarenumber + " url.");
                        }
                    }
                    else
                    {
                        Client.SendMessage(Client.JoinedChannels[0], "@" + e.Command.ChatMessage.Username + " the queue is currently closed for submissions!");
                    }
                    break;
                case "pos":
                case "position":
                    PositionInGamequeue(e.Command.ChatMessage.Username);
                    break;
                case "list":
                case "queue":
                    Client.SendMessage(Client.JoinedChannels[0], "@" + e.Command.ChatMessage.Username + " the queue currently have " + GameQueue.Count + " games in it!");
                    break;
                case "remove":
                case "delist":
                    RemoveGame(e.Command.ChatMessage.Username);
                    Properties.Settings.Default.SaveListXml = SerializeObject(GameQueue);
                    Properties.Settings.Default.Save();
                    break;
                case "mygame":
                case "submission":
                    Client.SendMessage(Client.JoinedChannels[0], "@" + e.Command.ChatMessage.Username + " You can play my game here: " + mygameUrl);
                    break;
                case "game":
                    AnnounceCurrentGame();
                    break;
                case "help":
                    ShowHelp(e.Command.ChatMessage.Username);
                    break;
            }
            Console.WriteLine(e.Command.CommandText);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Hide();
            this.WindowState = FormWindowState.Minimized;
            Console.WriteLine("Loaded");
        }

        private void SubmitGame(string url, string username)
        {
            if (url.Contains("ldjam.com/events/ludum-dare/" + ludumdarenumber + "/"))
            {
                if (GameQueue.Where(g => g.User == username).Count() != 0)
                {
                    Client.SendMessage(Client.JoinedChannels[0], "@" + username + " it seems you already have submitted a game, if you wanna submit an other you must first remove your game from the queue using !remove , you'll lose your place in the queue!");
                    return;
                }
                string[] splits = url.Split('/');
                string name = splits[splits.Length - 1];
                name = name.Replace('-', ' ');
                name = char.ToUpper(name[0]).ToString() + name.Substring(1);
                GameQueue.Add(new LdGame() { User = username, Name = name, Url = url });

                Client.SendMessage(Client.JoinedChannels[0], "@" + username + " " + name + " was added to the queue at positon " + GameQueue.Count + " in the queue.");
            }
            else
            {
                Client.SendMessage(Client.JoinedChannels[0], "@" + username + " it seems you have submitted a invalid URL, the url MUST be a Ldjam" + ludumdarenumber + " url.");
            }
        }

        private void RemoveGame(string username)
        {
            if (GameQueue.Where(g => g.User == username).Count() != 0)
            {
                GameQueue.RemoveAt(GameQueue.FindIndex(g => g.User == username));
                Client.SendMessage(Client.JoinedChannels[0], "@" + username + " your game have been removed!");
            }
            else
            {
                Client.SendMessage(Client.JoinedChannels[0], "@" + username + " it seems you have not submitted a game yet!");
            }
        }

        private void PositionInGamequeue(string username)
        {
            if (GameQueue.Where(g => g.User == username).Count() != 0)
            {
                Client.SendMessage(Client.JoinedChannels[0], "@" + username + " your game is number " + (GameQueue.FindIndex(g => g.User == username) + 1) + " in the queue!");
            }
            else
            {
                Client.SendMessage(Client.JoinedChannels[0], "@" + username + " it seems you have not submitted a game yet!");
            }
        }

        private void AnnounceCurrentGame()
        {
            Client.SendMessage(Client.JoinedChannels[0], "Now playing " + CurrentGame.Name + " by " + CurrentGame.User + " play along here: " + CurrentGame.Url);
        }

        private void ShowHelp(string username)
        {
            Client.SendMessage(Client.JoinedChannels[0], "@" + username + " use !submit [full link to your LD-" + ludumdarenumber + " game] to submit your game. Use !game to see current game being played. Type !play to play my game :D. You can remove your submission by using: !remove or see your current position in quest by typing !pos.");
        }

        public string SerializeObject<T>(T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public T DeserializeObject<T>(string toDeserialize)
        {
            if (string.IsNullOrEmpty(toDeserialize))
                return default(T);

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (StringReader textReader = new StringReader(toDeserialize))
            {
                return (T)xmlSerializer.Deserialize(textReader);
            }
        }
    }
}
